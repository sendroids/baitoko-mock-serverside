//https://qiita.com/oz4you/items/3f3223048bd2109de47b

//モジュール参照
const fs = require('fs')
const bodyParser = require('body-parser')
const jsonServer = require('json-server')
const jwt = require('jsonwebtoken')

//JSON Serverで、利用するJSONファイルを設定
const server = jsonServer.create()
const router = jsonServer.router('./db.json')
const dbLatest = JSON.parse(fs.readFileSync('./db.json', 'UTF-8'));
const middlewares = [
    // jsonServer.defaults({ readOnly: true }), // PUT & POST & DELETE will not work!!!!
    jsonServer.defaults(),
    [
        (req, res, next) => {
            router.db.assign(dbLatest).write()
            next();
        }
    ]
];
server.use(middlewares);
server.use(jsonServer.rewriter({
    '/api/*': '/$1' // Convert /api/xxx to /xxx
}))
// 毎回Request毎で、ユーザDBファイル読み込み(ユーザ権限更新の簡易対応)
// const userdb = JSON.parse(fs.readFileSync('./db.json', 'UTF-8'))

// Support _js_true _js_false query
router.render = (req, res) => {
    let data = res.locals.data
    if (!req._parsedUrl.query) {
        res.send(data);
        return;
    }
    // e.g StartByEmployeeId=11&ResultTo_js_false&
    let queries = req._parsedUrl.query.split('&').filter(e => !!e);
    if (!queries) {
        res.send(data);
        return;
    }
    let result = [];
    let isJsConditionUsed = false
    queries.forEach(query => {
        if (query && query.endsWith('_js_false')) {
            let key = query.replace('_js_false', '')
            if (typeof data !== 'function' && data.length > 0) {
                if (isJsConditionUsed) {
                    result = result.filter(d => !!!d[key])
                } else {
                    result = result.concat(data.filter(d => !!!d[key]))
                }
            }
            isJsConditionUsed = true
        } else if (query && query.endsWith('_js_true')) {
            let key = query.replace('_js_true', '')
            if (typeof data !== 'function' && data.length > 0) {
                if (isJsConditionUsed) {
                    result = result.filter(d => !!d[key])
                } else {
                    result = result.concat(data.filter(d => !!d[key]))
                }
            }
            isJsConditionUsed = true
        }
    });

    if (isJsConditionUsed) {
        res.send(result)
    } else {
        // _js_false/_js_trueでFilter結果が0件となる場合、他の条件を続く。
        res.send(data)
    }
}

//JSONリクエスト対応
server.use(bodyParser.urlencoded({ extended: true }))
server.use(bodyParser.json())

//署名作成ワードと有効期限(1時間)
const SECRET_WORD = 'odd-job-@Jwt!&Secret^#'
const expiresIn = '24h'

//署名作成関数
const createToken = payload => jwt.sign(payload, SECRET_WORD, { expiresIn })

//署名検証関数（非同期）
const verifyToken = token =>
    new Promise((resolve, reject) =>
        jwt.verify(token, SECRET_WORD, (err, decode) =>
            decode !== undefined ? resolve(decode) : reject(err)
        )
    )

//ログイン関数 true:ok false:ng
const findUser = ({ username, password }) =>
    dbLatest.users.find(user => user.username === username && user.password === password)

//ログRouter
server.post('/client-logs', (req, res) => {
    console.error("  [CLIENT LOGS]", req.body)
    res.status(200).json({ status: 'LOG SUCCESS' })
})

//ログインRouter
server.post('/authenticate', (req, res) => {
    // console.log('  [debug] Auth@originalUrl:' + req.originalUrl + ' rewite to url:' + req.url + ' (With baseUrl:' + req.baseUrl + ')');

    const { username, password } = req.body

    const user = findUser({ username, password })
    //ログイン検証
    if (!user) {
        const status = 401
        const message = 'Incorrect username or password'
        res.status(status).json({ status, message })
        console.log(`  [warn] Incorrect username ${username} with password ${password}`);
        return
    }

    //ログイン成功時に認証トークンを発行(Same with BOS)
    // {"uId":8,"dId":3,"fId":1,"uName":"太郎","uRoles":["role_salesman","role_designer_manager","role_designer","role_worker_manager","role_worker_leader","role_worker","role_admin"],"token":"..."}
    const userId = user.id;
    const tokenObj = {
        username: user.username,
        roleList: user.roleList,
        userId: userId
    };
    const token = createToken(tokenObj);

    res.header.authorization = 'Bearer ' + token;
    res.status(200).json({ token, userId });
})


//認証が必要なRouter(ログイン以外全て)
server.use(/^(?!\/authenticate).*$/, async (req, res, next) => {
    // console.log('  [debug] @originalUrl:' + req.originalUrl + ' rewite to url:' + req.url + ' (With baseUrl:' + req.baseUrl + ')');
    if (req.originalUrl === '/db' || req.originalUrl === '/status') {
        // allow json server functions at http://localhost:8080
        next();
        return;
    }

    //認証ヘッダー形式検証
    if (req.headers.authorization === undefined || req.headers.authorization.split(' ')[0] !== 'Bearer') {
        const status = 401
        const message = 'Error in authorization format'
        res.status(status).json({ status, message })
        return
    }

    // Support simple mock for Postman
    const token = req.headers.authorization.split(' ')[1];
    if (token === 'fake-token') {
        next();
        return;
    }

    //認証トークンの検証
    try {
        await verifyToken(req.headers.authorization.split(' ')[1])
        next()
    } catch (err) {
        //失効している認証トークン
        const status = 401
        const message = 'Error access_token is revoked'
        res.status(status).json({ status, message })
    }
})

//認証機能付きのREST APIサーバ起動
const PORT = 8080;
server.use(router)
server.listen(PORT, () => {
    console.log('Run Auth API Server at http://localhost:' + PORT)
})
